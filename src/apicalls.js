const apiKey = 'fef427d3e1db5c7b16320b906c7b0537'
const apiToken = 'ATTA58eaeb8cd3970f9b59879dc61138c44d77078b34efff2d0e27b8b5e8cf964b041F063845'



function getAllBoards() {
    const idmember = `662f78917455c90545182535`
    // const idmember = `662f9441d0172006b8b3f7e7`
    return fetch(`https://api.trello.com/1/members/${idmember}/boards?key=${apiKey}&token=${apiToken}`, {
        method: 'GET'
    })
        .then(response => {
            return response.json();
        })
}

function getBoardData(id){
    return fetch(`https://api.trello.com/1/boards/${id}?key=${apiKey}&token=${apiToken}`)
  .then(response => {
    return response.json();
  })
}

function createBoard(boardName) {
    let urlPath = `https://api.trello.com/1/boards/?name=${boardName}&key=${apiKey}&token=${apiToken}`
    return fetch(urlPath, {
        method: "POST"
    })
        .then((res) => {
            return res.json()
        })
}

function createNewCard(name, listId) {
    const url = `https://api.trello.com/1/cards?name=${name}&idList=${listId}&key=${apiKey}&token=${apiToken}`

    return fetch(url, { method: 'POST' })
        .then((res) => {
            return res.json();
        })
        .catch((err) => {
            console.log(err);
        })
}


function createNewLists(name, boardId) {

    return fetch(`https://api.trello.com/1/lists?name=${name}&idBoard=${boardId}&key=${apiKey}&token=${apiToken}`, {
        method: 'POST'
    })
        .then(response => {
            return response.json();
        })
        .catch(err => console.error(err));

}

function getLists(boardId) {

    const url = `https://api.trello.com/1/boards/${boardId}/lists?key=${apiKey}&token=${apiToken}`;
    return fetch(url, {
        method: 'GET'
    })
        .then(response => {
            return response.json();
        })
}

function getAllCards(listID) {
    const url = `https://api.trello.com/1/lists/${listID}/cards?key=${apiKey}&token=${apiToken}`;

    return fetch(url, {
        method: 'GET'
    })
        .then(response => {
            return response.json();
        })
}

function deleteCard(id) {
    return fetch(`https://api.trello.com/1/cards/${id}?key=${apiKey}&token=${apiToken}`, {
        method: 'DELETE'
    })
        .then((res) => {
            return res.json();
        })
        .catch((err) => {
            console.log(err);
        })
}
function deleteList(id) {
    const url = `https://api.trello.com/1/lists/${id}/closed?key=${apiKey}&token=${apiToken}&value=true`;

    return fetch(url, {
        method: 'PUT'
    })
        .then(response => {
            return response.json();
        })
        .catch(err => console.error(err));

}
function deleteBoard(id) {
    return fetch(`https://api.trello.com/1/boards/${id}?key=${apiKey}&token=${apiToken}`, {
        method: 'DELETE'
    })
    .then(response => {
        return response.json();
    })
}

export { getBoardData, getAllBoards, createBoard, createNewCard, createNewLists, getLists, getAllCards, deleteCard, deleteList, deleteBoard }